#include "mylibclass.hpp"
#include <iostream>

int main()
{
    MyLibClass mlc = MyLibClass();
    
    std::cout << "Calling Lib getVal(): " << mlc.getVal() << std::endl;
    
    std::cout << "Calling Lib setVal(5)" << std::endl; 
    mlc.setVal(5);
        
    std::cout << "Calling Lib getVal(): " << mlc.getVal() << std::endl;
}