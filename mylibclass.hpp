class MyLibClass {
  
public:
    MyLibClass();
    int getVal();
    void setVal(int i);
    
private:
    int value;    
};