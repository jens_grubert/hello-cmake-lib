#include "mylibclass.hpp"


MyLibClass::MyLibClass() {
    value = 0;
}

void MyLibClass::setVal(int i) {
    value = i;
}

int MyLibClass::getVal() {
    return value;
}
